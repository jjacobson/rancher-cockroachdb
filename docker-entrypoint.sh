#!/bin/bash
set -e

CMD=("$@")

CONTAINER_NAME=$(curl -sf http://rancher-metadata/2015-12-19/self/container/name)
STACK_NAME=$(curl -sf http://rancher-metadata/2015-12-19/self/stack/name)
SERVICE_NAME=$(curl -sf http://rancher-metadata/2015-12-19/self/service/name)


if [ -z "$STACK_NAME" ]; then
    echo "Stack name is empty"
    exit 1
fi

if [ -z "$SERVICE_NAME" ]; then
    echo "Service name is empty"
    exit 1
fi

MASTER_NAME=$(curl -sf "http://rancher-metadata/2015-12-19/stacks/${STACK_NAME}/services/${SERVICE_NAME}/containers/0/name")
if [ ! -z "$MASTER_NAME" ] && [ "$CONTAINER_NAME" != "$MASTER_NAME" ]; then
    echo "Joining cluster with ${MASTER_NAME}"
    CMD+=("--join=${MASTER_NAME}:26257")
else
    echo "First node in the cluster"
fi

DATA_DIR="/cockroach/cockroach-data/${CONTAINER_NAME}"
mkdir -p $DATA_DIR
CMD+=("--store=path=${DATA_DIR}")

exec /cockroach/cockroach "${CMD[@]}"
