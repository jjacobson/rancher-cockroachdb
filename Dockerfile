FROM cockroachdb/cockroach:v1.1.2

RUN apt-get update \
    && apt-get install -y curl --no-install-recommends

COPY docker-entrypoint.sh /

ENTRYPOINT ["/docker-entrypoint.sh"]
